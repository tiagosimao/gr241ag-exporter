package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// any number of hifens '--------------------------------------------------'
var separator = regexp.MustCompile(`^-+$`)

// captures the interface name
// Interface:   veip0.1
// eth3 [LAN4]
var interfaceName = regexp.MustCompile(`^Interface:\s+([^- ].*)$|^([^- ][^:]+)$`)

// captures the interface description
// Description: bridge_mode.12
var interfaceDescription = regexp.MustCompile(`^Description:\s+(.*)$`)

// captures stat sections
var interfacePacketType = regexp.MustCompile(`^\s{2}([^ ]+)$`) // received | transmitted

// captures name of the stat, value and unit
// Total                          47388964 bytes
// Frame 256 B ~ 511 B            69385 pkts
var intMetricWithUnit = regexp.MustCompile(`^\s+([^ ].+?) {2,}(-{0,1}\d+) ([^ ]+)$`)

// 0 errs
var looseIntMetricWithUnit = regexp.MustCompile(`^\s+(-{0,1}\d+) ([^ ]+)$`)

// Received Optical Level         -25.528 dBm
var floatMetricWithUnit = regexp.MustCompile(`^\s+([^ ].+?) {2,}(-{0,1}\d+\.\d+) ([^ ]+)$`)

// Link status                    Up
var stringMetricWithoutUnit = regexp.MustCompile(`^\s+([^ ].+?) {2,}([^ ]+)$`)

/*
const (
	parsingLanStatisticsStart                = 0
	parsingLanStatisticsInterfaceHeader      = 1
	parsingLanStatisticsInterfaceBody        = 2
	parsingLanStatisticsInterfaceReceived    = 3
	parsingLanStatisticsInterfaceTransmitted = 4
) */

func parseStatistics(lines []string) (Statistics, error) {
	result := Statistics{}

	var currentInterface *InterfaceStatistics = nil
	currentPacketType := ""

	for _, line := range lines {
		if currentInterface == nil {
			if separator.MatchString(line) {
				currentInterface = new(InterfaceStatistics)
			}
			continue // ignore all lines until separator
		}

		interfaceName, found := parseInterfaceName(line)
		if found {
			if currentInterface.Name != "" { // finished parsing previous interface
				result.Interfaces = append(result.Interfaces, *currentInterface)
			}
			currentInterface = new(InterfaceStatistics)
			currentInterface.Name = interfaceName
			continue
		} else if currentInterface == nil {
			continue
		}

		interfaceDescription, found := parseInterfaceDescription(line)
		if found {
			currentInterface.Description = interfaceDescription
			continue
		}

		if separator.MatchString(line) {
			currentPacketType = ""
		}

		statsPacketType, found := parsePacketType(line)
		if found {
			currentPacketType = statsPacketType
			continue
		}

		metric, found := parseMetric(line)
		if found {
			switch currentPacketType {
			case "Received":
				currentInterface.TrafficReceived = append(currentInterface.TrafficReceived, metric)
			case "Transmitted":
				currentInterface.TrafficTransmitted = append(currentInterface.TrafficTransmitted, metric)
			default:
				currentInterface.Status = append(currentInterface.Status, metric)
			}
		}
	}
	if currentInterface != nil {
		result.Interfaces = append(result.Interfaces, *currentInterface)
	}
	return result, nil
}

func parseInterfaceName(line string) (string, bool) {
	matches := interfaceName.FindStringSubmatch(line)
	if matches == nil || len(matches) < 2 {
		return "", false
	}
	for _, match := range matches[1:] {
		value := strings.TrimSpace(match)
		if value != "" {
			return value, true
		}
	}
	return "", false
}

func parseInterfaceDescription(line string) (string, bool) {
	matches := interfaceDescription.FindStringSubmatch(line)
	if matches == nil || len(matches) < 2 {
		return "", false
	}
	return strings.TrimSpace(matches[1]), true
}

func parsePacketType(line string) (string, bool) {
	matches := interfacePacketType.FindStringSubmatch(line)
	if matches == nil || len(matches) < 2 {
		return "", false
	}
	return strings.TrimSpace(matches[1]), true
}

func parseMetric(line string) (InterfaceMetric, bool) {
	metric, found := parseIntMetricWithUnit(line)
	if found {
		return metric, true
	}
	metric, found = parseFloatMetricWithUnit(line)
	if found {
		return metric, true
	}
	metric, found = parseLooseintMetricWithUnit(line)
	if found {
		return metric, true
	}
	metric, found = parseBooleanMetricWithoutUnit(line)
	if found {
		return metric, true
	}
	return InterfaceMetric{}, false
}

func parseIntMetricWithUnit(line string) (InterfaceMetric, bool) {
	matches := intMetricWithUnit.FindStringSubmatch(line)
	if matches == nil || len(matches) < 4 {
		return InterfaceMetric{}, false
	}
	name := matches[1]
	value, err := strconv.ParseInt(matches[2], 10, 64)
	if err != nil {
		fmt.Printf("error parsing number: %v\n", err)
		return InterfaceMetric{}, false
	}
	unit := matches[3]
	return InterfaceMetric{
		Name:  name,
		Value: float64(value),
		Unit:  unit,
	}, true
}

func parseLooseintMetricWithUnit(line string) (InterfaceMetric, bool) {
	matches := looseIntMetricWithUnit.FindStringSubmatch(line)
	if matches == nil || len(matches) < 3 {
		return InterfaceMetric{}, false
	}
	name := "Total"
	value, err := strconv.ParseInt(matches[1], 10, 64)
	if err != nil {
		fmt.Printf("error parsing number: %v\n", err)
		return InterfaceMetric{}, false
	}
	unit := matches[2]
	return InterfaceMetric{
		Name:  name,
		Value: float64(value),
		Unit:  unit,
	}, true
}

func parseFloatMetricWithUnit(line string) (InterfaceMetric, bool) {
	matches := floatMetricWithUnit.FindStringSubmatch(line)
	if matches == nil || len(matches) < 4 {
		return InterfaceMetric{}, false
	}
	name := matches[1]
	value, err := strconv.ParseFloat(matches[2], 64)
	if err != nil {
		fmt.Printf("error parsing number: %v\n", err)
		return InterfaceMetric{}, false
	}
	unit := matches[3]
	return InterfaceMetric{
		Name:  name,
		Value: value,
		Unit:  unit,
	}, true
}

func parseBooleanMetricWithoutUnit(line string) (InterfaceMetric, bool) {
	matches := stringMetricWithoutUnit.FindStringSubmatch(line)
	if matches == nil || len(matches) < 3 {
		return InterfaceMetric{}, false
	}
	name := matches[1]
	value := 0
	if matches[2] == "Up" {
		value = 1
	}
	return InterfaceMetric{
		Name:  name,
		Value: float64(value),
	}, true
}
