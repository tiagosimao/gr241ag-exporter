package main

type Config struct {
	Hostname string
	Port     uint16
	Username string
	Password string
	Ciphers  []string
}

type Statistics struct {
	Interfaces []InterfaceStatistics
}

// eth0 [LAN1], wl0 [Wi-Fi 2.4GHz], veip0...
type InterfaceStatistics struct {
	Name               string
	Description        string
	Status             []InterfaceMetric
	TrafficReceived    []InterfaceMetric
	TrafficTransmitted []InterfaceMetric
}

type InterfaceMetric struct {
	Name  string // total, multicast, ...
	Value float64
	Unit  string // bytes, pkts, dBm, Up, ...
}
