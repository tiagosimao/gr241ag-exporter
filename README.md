# GR241AG Exporter
A Prometheus exporter for the GR241AG router as provided by Altice to home users.

This was developed using a model branded as MEO FiberGateway.

## Usage
```sh
go run . 9090 192.168.1.254 22 <user> <pass>
```

Then check if it works by running
```sh
curl localhost:9090/metrics
```

You should get something like this
```
...
# HELP gr241_catv_optical_level_dbm 
# TYPE gr241_catv_optical_level_dbm gauge
gr241_catv_optical_level_dbm{interface="veip0",interface_type="optical"} -9.28
# HELP gr241_link_status 
# TYPE gr241_link_status gauge
gr241_link_status{interface="veip0",interface_type="optical"} 1
# HELP gr241_received_broadcast_pkts 
# TYPE gr241_received_broadcast_pkts gauge
gr241_received_broadcast_pkts{interface="eth0 [LAN1]",interface_type="lan"} 83
gr241_received_broadcast_pkts{interface="eth1 [LAN2]",interface_type="lan"} 22
gr241_received_broadcast_pkts{interface="eth2 [LAN3]",interface_type="lan"} 3797
gr241_received_broadcast_pkts{interface="eth3 [LAN4]",interface_type="lan"} 7
gr241_received_broadcast_pkts{interface="veip0.1",interface_type="wan"} 0
gr241_received_broadcast_pkts{interface="veip0.2",interface_type="wan"} 0
gr241_received_broadcast_pkts{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 0
gr241_received_broadcast_pkts{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 0
# HELP gr241_received_control_frame_pkts 
# TYPE gr241_received_control_frame_pkts gauge
gr241_received_control_frame_pkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_received_control_frame_pkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_received_control_frame_pkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_received_control_frame_pkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_received_fcs_frame_pkts 
# TYPE gr241_received_fcs_frame_pkts gauge
gr241_received_fcs_frame_pkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_received_fcs_frame_pkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_received_fcs_frame_pkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_received_fcs_frame_pkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_received_fragments_pkts 
# TYPE gr241_received_fragments_pkts gauge
gr241_received_fragments_pkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_received_fragments_pkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_received_fragments_pkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_received_fragments_pkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_received_frame_1024_b1518_b_pkts 
# TYPE gr241_received_frame_1024_b1518_b_pkts gauge
gr241_received_frame_1024_b1518_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 157284
gr241_received_frame_1024_b1518_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 318
gr241_received_frame_1024_b1518_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 190
gr241_received_frame_1024_b1518_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 14
# HELP gr241_received_frame_128_b255_b_pkts 
# TYPE gr241_received_frame_128_b255_b_pkts gauge
gr241_received_frame_128_b255_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 16321
gr241_received_frame_128_b255_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 1252
gr241_received_frame_128_b255_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 2823
gr241_received_frame_128_b255_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 1017
# HELP gr241_received_frame_1519_bmtu_pkts 
# TYPE gr241_received_frame_1519_bmtu_pkts gauge
gr241_received_frame_1519_bmtu_pkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_received_frame_1519_bmtu_pkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_received_frame_1519_bmtu_pkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_received_frame_1519_bmtu_pkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_received_frame_256_b511_b_pkts 
# TYPE gr241_received_frame_256_b511_b_pkts gauge
gr241_received_frame_256_b511_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 19976
gr241_received_frame_256_b511_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 747
gr241_received_frame_256_b511_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 5857
gr241_received_frame_256_b511_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 5731
# HELP gr241_received_frame_512_b1023_b_pkts 
# TYPE gr241_received_frame_512_b1023_b_pkts gauge
gr241_received_frame_512_b1023_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 11438
gr241_received_frame_512_b1023_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 1484
gr241_received_frame_512_b1023_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 1278
gr241_received_frame_512_b1023_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 461
# HELP gr241_received_frame_64_b_pkts 
# TYPE gr241_received_frame_64_b_pkts gauge
gr241_received_frame_64_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 8754
gr241_received_frame_64_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 1715
gr241_received_frame_64_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 3176
gr241_received_frame_64_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 2622
# HELP gr241_received_frame_65_b127_b_pkts 
# TYPE gr241_received_frame_65_b127_b_pkts gauge
gr241_received_frame_65_b127_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 136823
gr241_received_frame_65_b127_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 19261
gr241_received_frame_65_b127_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 3839
gr241_received_frame_65_b127_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 991
# HELP gr241_received_jabber_frame_pkts 
# TYPE gr241_received_jabber_frame_pkts gauge
gr241_received_jabber_frame_pkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_received_jabber_frame_pkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_received_jabber_frame_pkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_received_jabber_frame_pkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_received_multicast_pkts 
# TYPE gr241_received_multicast_pkts gauge
gr241_received_multicast_pkts{interface="eth0 [LAN1]",interface_type="lan"} 11889
gr241_received_multicast_pkts{interface="eth1 [LAN2]",interface_type="lan"} 639
gr241_received_multicast_pkts{interface="eth2 [LAN3]",interface_type="lan"} 4185
gr241_received_multicast_pkts{interface="eth3 [LAN4]",interface_type="lan"} 4071
gr241_received_multicast_pkts{interface="veip0.1",interface_type="wan"} 683207
gr241_received_multicast_pkts{interface="veip0.2",interface_type="wan"} 87
gr241_received_multicast_pkts{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 0
gr241_received_multicast_pkts{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 0
# HELP gr241_received_optical_level_dbm 
# TYPE gr241_received_optical_level_dbm gauge
gr241_received_optical_level_dbm{interface="veip0",interface_type="optical"} -26.02
# HELP gr241_received_oversize_packetmtupkts 
# TYPE gr241_received_oversize_packetmtupkts gauge
gr241_received_oversize_packetmtupkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_received_oversize_packetmtupkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_received_oversize_packetmtupkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_received_oversize_packetmtupkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_received_pause_control_frame_pkts 
# TYPE gr241_received_pause_control_frame_pkts gauge
gr241_received_pause_control_frame_pkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_received_pause_control_frame_pkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_received_pause_control_frame_pkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_received_pause_control_frame_pkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_received_total_bytes 
# TYPE gr241_received_total_bytes gauge
gr241_received_total_bytes{interface="eth0 [LAN1]",interface_type="lan"} 2.61465158e+08
gr241_received_total_bytes{interface="eth1 [LAN2]",interface_type="lan"} 3.304667e+06
gr241_received_total_bytes{interface="eth2 [LAN3]",interface_type="lan"} 4.442365e+06
gr241_received_total_bytes{interface="eth3 [LAN4]",interface_type="lan"} 3.38289e+06
gr241_received_total_bytes{interface="veip0",interface_type="optical"} 2.157625114e+09
gr241_received_total_bytes{interface="veip0.1",interface_type="wan"} 1.395914482e+09
gr241_received_total_bytes{interface="veip0.2",interface_type="wan"} 4002
gr241_received_total_bytes{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 8.1332826e+07
gr241_received_total_bytes{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 0
# HELP gr241_received_total_drops 
# TYPE gr241_received_total_drops gauge
gr241_received_total_drops{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_received_total_drops{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_received_total_drops{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_received_total_drops{interface="eth3 [LAN4]",interface_type="lan"} 0
gr241_received_total_drops{interface="veip0",interface_type="optical"} 23113
gr241_received_total_drops{interface="veip0.1",interface_type="wan"} 0
gr241_received_total_drops{interface="veip0.2",interface_type="wan"} 0
gr241_received_total_drops{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 154
gr241_received_total_drops{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 26
# HELP gr241_received_total_errs 
# TYPE gr241_received_total_errs gauge
gr241_received_total_errs{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_received_total_errs{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_received_total_errs{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_received_total_errs{interface="eth3 [LAN4]",interface_type="lan"} 0
gr241_received_total_errs{interface="veip0",interface_type="optical"} 0
gr241_received_total_errs{interface="veip0.1",interface_type="wan"} 0
gr241_received_total_errs{interface="veip0.2",interface_type="wan"} 0
gr241_received_total_errs{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 0
gr241_received_total_errs{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 0
# HELP gr241_received_total_pkts 
# TYPE gr241_received_total_pkts gauge
gr241_received_total_pkts{interface="eth0 [LAN1]",interface_type="lan"} 350596
gr241_received_total_pkts{interface="eth1 [LAN2]",interface_type="lan"} 24777
gr241_received_total_pkts{interface="eth2 [LAN3]",interface_type="lan"} 17163
gr241_received_total_pkts{interface="eth3 [LAN4]",interface_type="lan"} 10836
gr241_received_total_pkts{interface="veip0",interface_type="optical"} 1.8363e+06
gr241_received_total_pkts{interface="veip0.1",interface_type="wan"} 1.314499e+06
gr241_received_total_pkts{interface="veip0.2",interface_type="wan"} 87
gr241_received_total_pkts{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 394732
gr241_received_total_pkts{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 0
# HELP gr241_received_unicast_pkts 
# TYPE gr241_received_unicast_pkts gauge
gr241_received_unicast_pkts{interface="eth0 [LAN1]",interface_type="lan"} 338624
gr241_received_unicast_pkts{interface="eth1 [LAN2]",interface_type="lan"} 24116
gr241_received_unicast_pkts{interface="eth2 [LAN3]",interface_type="lan"} 9181
gr241_received_unicast_pkts{interface="eth3 [LAN4]",interface_type="lan"} 6758
gr241_received_unicast_pkts{interface="veip0.1",interface_type="wan"} 1.3144e+06
gr241_received_unicast_pkts{interface="veip0.2",interface_type="wan"} 0
gr241_received_unicast_pkts{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 394732
gr241_received_unicast_pkts{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 0
# HELP gr241_received_unknown_errors_pkts 
# TYPE gr241_received_unknown_errors_pkts gauge
gr241_received_unknown_errors_pkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_received_unknown_errors_pkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_received_unknown_errors_pkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_received_unknown_errors_pkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_transmited_optical_level_dbm 
# TYPE gr241_transmited_optical_level_dbm gauge
gr241_transmited_optical_level_dbm{interface="veip0",interface_type="optical"} 2.4
# HELP gr241_transmitted_broadcast_pkts 
# TYPE gr241_transmitted_broadcast_pkts gauge
gr241_transmitted_broadcast_pkts{interface="eth0 [LAN1]",interface_type="lan"} 4122
gr241_transmitted_broadcast_pkts{interface="eth1 [LAN2]",interface_type="lan"} 4183
gr241_transmitted_broadcast_pkts{interface="eth2 [LAN3]",interface_type="lan"} 268
gr241_transmitted_broadcast_pkts{interface="eth3 [LAN4]",interface_type="lan"} 4196
gr241_transmitted_broadcast_pkts{interface="veip0.1",interface_type="wan"} 12
gr241_transmitted_broadcast_pkts{interface="veip0.2",interface_type="wan"} 12
gr241_transmitted_broadcast_pkts{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 0
gr241_transmitted_broadcast_pkts{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 0
# HELP gr241_transmitted_control_frame_pkts 
# TYPE gr241_transmitted_control_frame_pkts gauge
gr241_transmitted_control_frame_pkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_transmitted_control_frame_pkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_transmitted_control_frame_pkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_transmitted_control_frame_pkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_transmitted_deferral_packet_pkts 
# TYPE gr241_transmitted_deferral_packet_pkts gauge
gr241_transmitted_deferral_packet_pkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_transmitted_deferral_packet_pkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_transmitted_deferral_packet_pkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_transmitted_deferral_packet_pkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_transmitted_fcs_frame 
# TYPE gr241_transmitted_fcs_frame gauge
gr241_transmitted_fcs_frame{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_transmitted_fcs_frame{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_transmitted_fcs_frame{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_transmitted_fcs_frame{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_transmitted_fragments 
# TYPE gr241_transmitted_fragments gauge
gr241_transmitted_fragments{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_transmitted_fragments{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_transmitted_fragments{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_transmitted_fragments{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_transmitted_frame_1024_b1518_b_pkts 
# TYPE gr241_transmitted_frame_1024_b1518_b_pkts gauge
gr241_transmitted_frame_1024_b1518_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 21245
gr241_transmitted_frame_1024_b1518_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 2156
gr241_transmitted_frame_1024_b1518_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 1063
gr241_transmitted_frame_1024_b1518_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 688815
# HELP gr241_transmitted_frame_128_b255_b_pkts 
# TYPE gr241_transmitted_frame_128_b255_b_pkts gauge
gr241_transmitted_frame_128_b255_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 35681
gr241_transmitted_frame_128_b255_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 7886
gr241_transmitted_frame_128_b255_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 1883
gr241_transmitted_frame_128_b255_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 7418
# HELP gr241_transmitted_frame_1519_bmtu_pkts 
# TYPE gr241_transmitted_frame_1519_bmtu_pkts gauge
gr241_transmitted_frame_1519_bmtu_pkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_transmitted_frame_1519_bmtu_pkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_transmitted_frame_1519_bmtu_pkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_transmitted_frame_1519_bmtu_pkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_transmitted_frame_256_b511_b_pkts 
# TYPE gr241_transmitted_frame_256_b511_b_pkts gauge
gr241_transmitted_frame_256_b511_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 26386
gr241_transmitted_frame_256_b511_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 12586
gr241_transmitted_frame_256_b511_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 5989
gr241_transmitted_frame_256_b511_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 9261
# HELP gr241_transmitted_frame_512_b1023_b_pkts 
# TYPE gr241_transmitted_frame_512_b1023_b_pkts gauge
gr241_transmitted_frame_512_b1023_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 10973
gr241_transmitted_frame_512_b1023_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 3696
gr241_transmitted_frame_512_b1023_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 1551
gr241_transmitted_frame_512_b1023_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 2303
# HELP gr241_transmitted_frame_64_b_pkts 
# TYPE gr241_transmitted_frame_64_b_pkts gauge
gr241_transmitted_frame_64_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 11482
gr241_transmitted_frame_64_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 18375
gr241_transmitted_frame_64_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 6514
gr241_transmitted_frame_64_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 11484
# HELP gr241_transmitted_frame_65127_b_pkts 
# TYPE gr241_transmitted_frame_65127_b_pkts gauge
gr241_transmitted_frame_65127_b_pkts{interface="eth0 [LAN1]",interface_type="lan"} 172728
gr241_transmitted_frame_65127_b_pkts{interface="eth1 [LAN2]",interface_type="lan"} 11395
gr241_transmitted_frame_65127_b_pkts{interface="eth2 [LAN3]",interface_type="lan"} 1954
gr241_transmitted_frame_65127_b_pkts{interface="eth3 [LAN4]",interface_type="lan"} 5698
# HELP gr241_transmitted_multicast_pkts 
# TYPE gr241_transmitted_multicast_pkts gauge
gr241_transmitted_multicast_pkts{interface="eth0 [LAN1]",interface_type="lan"} 19238
gr241_transmitted_multicast_pkts{interface="eth1 [LAN2]",interface_type="lan"} 29599
gr241_transmitted_multicast_pkts{interface="eth2 [LAN3]",interface_type="lan"} 12944
gr241_transmitted_multicast_pkts{interface="eth3 [LAN4]",interface_type="lan"} 709008
gr241_transmitted_multicast_pkts{interface="veip0.1",interface_type="wan"} 153
gr241_transmitted_multicast_pkts{interface="veip0.2",interface_type="wan"} 153
gr241_transmitted_multicast_pkts{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 0
gr241_transmitted_multicast_pkts{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 18894
# HELP gr241_transmitted_oversize_packetmtu 
# TYPE gr241_transmitted_oversize_packetmtu gauge
gr241_transmitted_oversize_packetmtu{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_transmitted_oversize_packetmtu{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_transmitted_oversize_packetmtu{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_transmitted_oversize_packetmtu{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_transmitted_pause_control_frame_pkts 
# TYPE gr241_transmitted_pause_control_frame_pkts gauge
gr241_transmitted_pause_control_frame_pkts{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_transmitted_pause_control_frame_pkts{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_transmitted_pause_control_frame_pkts{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_transmitted_pause_control_frame_pkts{interface="eth3 [LAN4]",interface_type="lan"} 0
# HELP gr241_transmitted_total_bytes 
# TYPE gr241_transmitted_total_bytes gauge
gr241_transmitted_total_bytes{interface="eth0 [LAN1]",interface_type="lan"} 6.9537197e+07
gr241_transmitted_total_bytes{interface="eth1 [LAN2]",interface_type="lan"} 1.4882252e+07
gr241_transmitted_total_bytes{interface="eth2 [LAN3]",interface_type="lan"} 6.179107e+06
gr241_transmitted_total_bytes{interface="eth3 [LAN4]",interface_type="lan"} 9.54843937e+08
gr241_transmitted_total_bytes{interface="veip0",interface_type="optical"} 1.39628283e+08
gr241_transmitted_total_bytes{interface="veip0.1",interface_type="wan"} 2.79222198e+08
gr241_transmitted_total_bytes{interface="veip0.2",interface_type="wan"} 1598
gr241_transmitted_total_bytes{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 1.352177712e+09
gr241_transmitted_total_bytes{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 3.261733e+06
# HELP gr241_transmitted_total_drops 
# TYPE gr241_transmitted_total_drops gauge
gr241_transmitted_total_drops{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_transmitted_total_drops{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_transmitted_total_drops{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_transmitted_total_drops{interface="eth3 [LAN4]",interface_type="lan"} 0
gr241_transmitted_total_drops{interface="veip0",interface_type="optical"} 0
gr241_transmitted_total_drops{interface="veip0.1",interface_type="wan"} 0
gr241_transmitted_total_drops{interface="veip0.2",interface_type="wan"} 0
gr241_transmitted_total_drops{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 0
gr241_transmitted_total_drops{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 365
# HELP gr241_transmitted_total_errs 
# TYPE gr241_transmitted_total_errs gauge
gr241_transmitted_total_errs{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_transmitted_total_errs{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_transmitted_total_errs{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_transmitted_total_errs{interface="eth3 [LAN4]",interface_type="lan"} 0
gr241_transmitted_total_errs{interface="veip0",interface_type="optical"} 0
gr241_transmitted_total_errs{interface="veip0.1",interface_type="wan"} 0
gr241_transmitted_total_errs{interface="veip0.2",interface_type="wan"} 0
gr241_transmitted_total_errs{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 1007
gr241_transmitted_total_errs{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 0
# HELP gr241_transmitted_total_pkts 
# TYPE gr241_transmitted_total_pkts gauge
gr241_transmitted_total_pkts{interface="eth0 [LAN1]",interface_type="lan"} 278495
gr241_transmitted_total_pkts{interface="eth1 [LAN2]",interface_type="lan"} 56094
gr241_transmitted_total_pkts{interface="eth2 [LAN3]",interface_type="lan"} 18954
gr241_transmitted_total_pkts{interface="eth3 [LAN4]",interface_type="lan"} 724979
gr241_transmitted_total_pkts{interface="veip0",interface_type="optical"} 502504
gr241_transmitted_total_pkts{interface="veip0.1",interface_type="wan"} 614453
gr241_transmitted_total_pkts{interface="veip0.2",interface_type="wan"} 17
gr241_transmitted_total_pkts{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 1.157431e+06
gr241_transmitted_total_pkts{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 18894
# HELP gr241_transmitted_unicast_pkts 
# TYPE gr241_transmitted_unicast_pkts gauge
gr241_transmitted_unicast_pkts{interface="eth0 [LAN1]",interface_type="lan"} 255135
gr241_transmitted_unicast_pkts{interface="eth1 [LAN2]",interface_type="lan"} 22312
gr241_transmitted_unicast_pkts{interface="eth2 [LAN3]",interface_type="lan"} 5742
gr241_transmitted_unicast_pkts{interface="eth3 [LAN4]",interface_type="lan"} 11775
gr241_transmitted_unicast_pkts{interface="veip0.1",interface_type="wan"} 614288
gr241_transmitted_unicast_pkts{interface="veip0.2",interface_type="wan"} 0
gr241_transmitted_unicast_pkts{interface="wl0 [Wi-Fi 2.4GHz]",interface_type="lan"} 1.157431e+06
gr241_transmitted_unicast_pkts{interface="wl1 [Wi-Fi 5GHz]",interface_type="lan"} 0
# HELP gr241_transmitted_unknown_errors 
# TYPE gr241_transmitted_unknown_errors gauge
gr241_transmitted_unknown_errors{interface="eth0 [LAN1]",interface_type="lan"} 0
gr241_transmitted_unknown_errors{interface="eth1 [LAN2]",interface_type="lan"} 0
gr241_transmitted_unknown_errors{interface="eth2 [LAN3]",interface_type="lan"} 0
gr241_transmitted_unknown_errors{interface="eth3 [LAN4]",interface_type="lan"} 0
...
```

