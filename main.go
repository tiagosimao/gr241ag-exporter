package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	listenPort := os.Args[1]
	deviceIp := os.Args[2]
	devicePortString := os.Args[3]
	deviceUser := os.Args[4]
	devicePass := os.Args[5]

	devicePort, err := strconv.Atoi(devicePortString)
	if err != nil {
		devicePort = 22
	}

	exporter := GR241AG{
		Config: Config{
			Hostname: deviceIp,
			Port:     uint16(devicePort),
			Username: deviceUser,
			Password: devicePass,
		},
	}
	prometheus.MustRegister(&exporter)

	http.Handle("/metrics", promhttp.Handler())
	panic(http.ListenAndServe(fmt.Sprintf(":%s", listenPort), nil))

}
