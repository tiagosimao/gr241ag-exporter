FROM golang:1.19.1-alpine3.16 as build
ENV CGO_ENABLED=0
WORKDIR /src
RUN printf "app:x:1000:1000:Linux User,,,:/_:/bin/ash" > passwd
COPY . .
RUN GOARCH=arm64 go build -ldflags "-w -s" -o arm64/app .
RUN GOARCH=amd64 go build -ldflags "-w -s" -o amd64/app .

FROM scratch
ARG TARGETARCH
COPY --from=build /src/passwd /etc/passwd
USER app
COPY --from=build /src/${TARGETARCH}/app .
ENTRYPOINT ["/app"]