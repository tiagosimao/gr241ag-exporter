package main

import (
	"fmt"
	"io"
	"regexp"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"golang.org/x/crypto/ssh"
)

// matching weird chars
var re = regexp.MustCompile(`\s*[^a-z0-9 ]+\s*`)

type GR241AG struct {
	Config Config
}

func connect(config Config) (*ssh.Client, error) {
	sshConfig := &ssh.ClientConfig{
		User:            config.Username,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Auth:            []ssh.AuthMethod{ssh.Password(config.Password)},
		Timeout:         time.Second * 4,
	}
	sshConfig.Ciphers = append(sshConfig.Ciphers, "3des-cbc", "twofish128-cbc", "aes256-ctr", "twofish256-ctr")

	client, err := ssh.Dial("tcp", fmt.Sprintf("%s:%d", config.Hostname, config.Port), sshConfig)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func (collector *GR241AG) Describe(ch chan<- *prometheus.Desc) {
	// TODO bootstrap descriptions
}

func (collector *GR241AG) Collect(ch chan<- prometheus.Metric) {
	client, err := connect(collector.Config)
	if err != nil {
		fmt.Printf("error connecting to device: %v\n", err)
		return
	}
	defer client.Close()

	session, err := client.NewSession()
	if err != nil {
		fmt.Printf("error gettin session in device: %v\n", err)
		return
	}
	defer session.Close()

	stdin, err := session.StdinPipe()
	if err != nil {
		fmt.Printf("error getting stdin on device: %v\n", err)
		return
	}

	stdout, err := session.StdoutPipe()
	if err != nil {
		fmt.Printf("error getting stdout on device: %v\n", err)
		return
	}

	err = session.Shell()
	if err != nil {
		fmt.Printf("error getting shell on device: %v\n", err)
		return
	}

	_, err = runCommand("", stdin, stdout)
	if err != nil {
		fmt.Printf("error getting hello message on device: %v\n", err)
		return
	}

	collectStats("lan", stdin, stdout, ch)
	if err != nil {
		fmt.Printf("error collecting lan statistics: %v\n", err)
	}

	collectStats("wan", stdin, stdout, ch)
	if err != nil {
		fmt.Printf("error collecting wan statistics: %v\n", err)
	}

	collectStats("optical", stdin, stdout, ch)
	if err != nil {
		fmt.Printf("error collecting optical statistics: %v\n", err)
	}

}

func collectStats(statType string, stdin io.Writer, stdout io.Reader, ch chan<- prometheus.Metric) error {
	output, err := runCommand(fmt.Sprintf("statistics/%s/show --option=all", statType), stdin, stdout)
	if err != nil {
		return err
	}

	stats, err := parseStatistics(output)
	if err != nil {
		return err
	}

	for _, interfaceStats := range stats.Interfaces {
		labels := prometheus.Labels{
			"interface":      interfaceStats.Name,
			"interface_type": statType,
		}
		toPromMetrics("", interfaceStats.Status, labels, ch)
		toPromMetrics("received", interfaceStats.TrafficReceived, labels, ch)
		toPromMetrics("transmitted", interfaceStats.TrafficTransmitted, labels, ch)
	}
	return nil
}

func toPromMetrics(prefix string, metrics []InterfaceMetric, labels prometheus.Labels, ch chan<- prometheus.Metric) {
	for _, metric := range metrics {
		name := strings.TrimSpace(strings.ToLower(strings.Join([]string{prefix, metric.Name, metric.Unit}, " ")))
		name = strings.ReplaceAll(re.ReplaceAllString(name, ``), " ", "_")
		desc := prometheus.NewDesc(prometheus.BuildFQName("gr241ag", "", name), "", []string{}, labels)
		if desc == nil {
			fmt.Printf("error creating metric description: %s\n", name)
			continue
		}
		promMetric, err := prometheus.NewConstMetric(desc, prometheus.GaugeValue, float64(metric.Value))
		if err != nil {
			fmt.Printf("error creating metric: %v\n", err)
			continue
		}
		ch <- promMetric
	}
}

func runCommand(command string, stdin io.Writer, stdout io.Reader) ([]string, error) {
	output := []string{}
	if command != "" {
		_, err := stdin.Write([]byte(fmt.Sprintf("%s\n", command))) // TODO ensure everything was written
		if err != nil {
			return output, err
		}
	}
	ch := make(chan string)
	go readLines(stdout, ch)
	for line := range ch {
		output = append(output, line)
	}
	return output, nil
}

func readLines(stdout io.Reader, ch chan string) {
	buffer := make([]byte, 1024)
	partialLine := ""
	for {
		readByteCount, err := stdout.Read(buffer)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Printf("error parsing command output on device: %v\n", err)
			continue
		}
		if readByteCount > 0 {
			chunk := string(buffer[:readByteCount]) // assuming ASCII here (single byte chars)
			// parse completed lines
			newLineAt := strings.IndexAny(chunk, "\n")
			for newLineAt > -1 {
				if newLineAt > 0 {
					line := partialLine + chunk[:newLineAt]
					ch <- line
				} else if partialLine != "" {
					line := partialLine
					ch <- line
				}
				partialLine = "" // consumed above

				// more stuff in this chunk
				chunk = chunk[newLineAt+1:]
				newLineAt = strings.IndexAny(chunk, "\n")
			}

			eofAt := strings.Index(chunk, "/cli> ") // looking for /cli>, effectivelly the EOF we want
			if eofAt > -1 {
				line := partialLine + chunk[:eofAt]
				ch <- line
				break
			}

			if len(chunk) > 0 { // a partial line was left-over
				partialLine += chunk
			}

		}
	}
	close(ch)
}
